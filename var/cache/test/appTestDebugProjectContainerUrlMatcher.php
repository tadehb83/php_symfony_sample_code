<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appTestDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($rawPathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        elseif (0 === strpos($pathinfo, '/products')) {
            // ApiBundle_ProductController_post_products
            if (preg_match('#^/products(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_ApiBundle_ProductController_post_products;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_ProductController_post_products')), array (  '_controller' => 'ApiBundle\\Controller\\ProductController:postProductsAction',  '_format' => 'json',));
            }
            not_ApiBundle_ProductController_post_products:

            // ApiBundle_ProductController_get_products
            if (preg_match('#^/products(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_ApiBundle_ProductController_get_products;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_ProductController_get_products')), array (  '_controller' => 'ApiBundle\\Controller\\ProductController:getProductsAction',  '_format' => 'json',));
            }
            not_ApiBundle_ProductController_get_products:

            // ApiBundle_ProductController_get_product
            if (preg_match('#^/products/(?P<issn>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_ApiBundle_ProductController_get_product;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_ProductController_get_product')), array (  '_controller' => 'ApiBundle\\Controller\\ProductController:getProductAction',  '_format' => 'json',));
            }
            not_ApiBundle_ProductController_get_product:

            // ApiBundle_ProductController_put_products
            if (preg_match('#^/products/(?P<issn>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('PUT' !== $canonicalMethod) {
                    $allow[] = 'PUT';
                    goto not_ApiBundle_ProductController_put_products;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_ProductController_put_products')), array (  '_controller' => 'ApiBundle\\Controller\\ProductController:putProductsAction',  '_format' => 'json',));
            }
            not_ApiBundle_ProductController_put_products:

            // ApiBundle_ProductController_delete_products
            if (preg_match('#^/products/(?P<issn>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('DELETE' !== $canonicalMethod) {
                    $allow[] = 'DELETE';
                    goto not_ApiBundle_ProductController_delete_products;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_ProductController_delete_products')), array (  '_controller' => 'ApiBundle\\Controller\\ProductController:deleteProductsAction',  '_format' => 'json',));
            }
            not_ApiBundle_ProductController_delete_products:

        }

        elseif (0 === strpos($pathinfo, '/customers')) {
            // ApiBundle_CustomerController_post_customers
            if (preg_match('#^/customers(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_ApiBundle_CustomerController_post_customers;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_CustomerController_post_customers')), array (  '_controller' => 'ApiBundle\\Controller\\CustomerController:postCustomersAction',  '_format' => 'json',));
            }
            not_ApiBundle_CustomerController_post_customers:

            // ApiBundle_CustomerController_get_customers
            if (preg_match('#^/customers(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_ApiBundle_CustomerController_get_customers;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_CustomerController_get_customers')), array (  '_controller' => 'ApiBundle\\Controller\\CustomerController:getCustomersAction',  '_format' => 'json',));
            }
            not_ApiBundle_CustomerController_get_customers:

            // ApiBundle_CustomerController_get_customer
            if (preg_match('#^/customers/(?P<uuid>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_ApiBundle_CustomerController_get_customer;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_CustomerController_get_customer')), array (  '_controller' => 'ApiBundle\\Controller\\CustomerController:getCustomerAction',  '_format' => 'json',));
            }
            not_ApiBundle_CustomerController_get_customer:

            // ApiBundle_CustomerController_put_customers
            if (preg_match('#^/customers/(?P<uuid>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('PUT' !== $canonicalMethod) {
                    $allow[] = 'PUT';
                    goto not_ApiBundle_CustomerController_put_customers;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_CustomerController_put_customers')), array (  '_controller' => 'ApiBundle\\Controller\\CustomerController:putCustomersAction',  '_format' => 'json',));
            }
            not_ApiBundle_CustomerController_put_customers:

            // ApiBundle_CustomerController_delete_customers
            if (preg_match('#^/customers/(?P<uuid>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('DELETE' !== $canonicalMethod) {
                    $allow[] = 'DELETE';
                    goto not_ApiBundle_CustomerController_delete_customers;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_CustomerController_delete_customers')), array (  '_controller' => 'ApiBundle\\Controller\\CustomerController:deleteCustomersAction',  '_format' => 'json',));
            }
            not_ApiBundle_CustomerController_delete_customers:

            // ApiBundle_CustomerController_get_customers_products
            if (preg_match('#^/customers/(?P<customer_uuid>[^/]++)/products(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_ApiBundle_CustomerController_get_customers_products;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ApiBundle_CustomerController_get_customers_products')), array (  '_controller' => 'ApiBundle\\Controller\\CustomerController:getCustomersProductsAction',  '_format' => 'json',));
            }
            not_ApiBundle_CustomerController_get_customers_products:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
