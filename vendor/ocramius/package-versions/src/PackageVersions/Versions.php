<?php

namespace PackageVersions;

/**
 * This class is generated by ocramius/package-versions, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 */
final class Versions
{
    const ROOT_PACKAGE_NAME = 'sample rest api';
    const VERSIONS = array (
  'composer/ca-bundle' => '1.1.2@46afded9720f40b9dc63542af4e3e43a1177acb0',
  'doctrine/annotations' => 'v1.6.0@c7f2050c68a9ab0bdb0f98567ec08d80ea7d24d5',
  'doctrine/cache' => 'v1.8.0@d768d58baee9a4862ca783840eca1b9add7a7f57',
  'doctrine/collections' => 'v1.5.0@a01ee38fcd999f34d9bfbcee59dbda5105449cbf',
  'doctrine/common' => 'v2.9.0@a210246d286c77d2b89040f8691ba7b3a713d2c1',
  'doctrine/data-fixtures' => 'v1.3.1@3a1e2c3c600e615a2dffe56d4ca0875cc5233e0a',
  'doctrine/dbal' => 'v2.8.0@5140a64c08b4b607b9bedaae0cedd26f04a0e621',
  'doctrine/doctrine-bundle' => '1.9.1@703fad32e4c8cbe609caf45a71a1d4266c830f0f',
  'doctrine/doctrine-cache-bundle' => '1.3.3@4c8e363f96427924e7e519c5b5119b4f54512697',
  'doctrine/doctrine-fixtures-bundle' => '3.0.2@7fc29d2b18c61ed99826b21fbfd1ff9969cc2e7f',
  'doctrine/doctrine-migrations-bundle' => 'v1.3.1@a9e506369f931351a2a6dd2aef588a822802b1b7',
  'doctrine/event-manager' => 'v1.0.0@a520bc093a0170feeb6b14e9d83f3a14452e64b3',
  'doctrine/inflector' => 'v1.3.0@5527a48b7313d15261292c149e55e26eae771b0a',
  'doctrine/instantiator' => '1.1.0@185b8868aa9bf7159f5f953ed5afb2d7fcdc3bda',
  'doctrine/lexer' => 'v1.0.1@83893c552fd2045dd78aef794c31e694c37c0b8c',
  'doctrine/migrations' => 'v1.8.1@215438c0eef3e5f9b7da7d09c6b90756071b43e6',
  'doctrine/orm' => 'v2.6.2@d2b4dd71d2a276edd65d0c170375b445f8a4a4a8',
  'doctrine/persistence' => 'v1.0.1@af1ec238659a83e320f03e0e454e200f689b4b97',
  'doctrine/reflection' => 'v1.0.0@02538d3f95e88eb397a5f86274deb2c6175c2ab6',
  'fig/link-util' => '1.0.0@1a07821801a148be4add11ab0603e4af55a72fac',
  'friendsofsymfony/oauth-server-bundle' => '1.6.1@5cc4c8555dedb5c7e737a35789bd429ccd9ad254',
  'friendsofsymfony/oauth2-php' => '1.2.3@a41fef63f81ef2ef632350a6c7dc66d15baa9240',
  'friendsofsymfony/rest-bundle' => '2.4.0@3725279a6554c1d894eee0b377f49d3f69e3c217',
  'friendsofsymfony/user-bundle' => 'v2.1.2@1049935edd24ec305cc6cfde1875372fa9600446',
  'incenteev/composer-parameter-handler' => 'v2.1.3@933c45a34814f27f2345c11c37d46b3ca7303550',
  'jdorn/sql-formatter' => 'v1.2.17@64990d96e0959dff8e059dfcdc1af130728d92bc',
  'jms/metadata' => '1.6.0@6a06970a10e0a532fb52d3959547123b84a3b3ab',
  'jms/parser-lib' => '1.0.0@c509473bc1b4866415627af0e1c6cc8ac97fa51d',
  'jms/serializer' => '1.13.0@00863e1d55b411cc33ad3e1de09a4c8d3aae793c',
  'jms/serializer-bundle' => '2.4.2@22eb9a2f7983394b0770237ca91e879eb2a4b4a6',
  'monolog/monolog' => '1.23.0@fd8c787753b3a2ad11bc60c063cff1358a32a3b4',
  'ocramius/package-versions' => '1.3.0@4489d5002c49d55576fa0ba786f42dbb009be46f',
  'ocramius/proxy-manager' => '2.2.2@14b137b06b0f911944132df9d51e445a35920ab1',
  'paragonie/random_compat' => 'v2.0.17@29af24f25bab834fcbb38ad2a69fa93b867e070d',
  'phpcollection/phpcollection' => '0.5.0@f2bcff45c0da7c27991bbc1f90f47c4b7fb434a6',
  'phpdocumentor/reflection-common' => '1.0.1@21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6',
  'phpdocumentor/reflection-docblock' => '4.3.0@94fd0001232e47129dd3504189fa1c7225010d08',
  'phpdocumentor/type-resolver' => '0.4.0@9c977708995954784726e25d0cd1dddf4e65b0f7',
  'phpoption/phpoption' => '1.5.0@94e644f7d2051a5f0fcf77d81605f152eecff0ed',
  'phpspec/prophecy' => '1.8.0@4ba436b55987b4bf311cb7c6ba82aa528aac0a06',
  'phpunit/php-code-coverage' => '2.2.4@eabf68b476ac7d0f73793aada060f1c1a9bf8979',
  'phpunit/php-file-iterator' => '1.4.5@730b01bc3e867237eaac355e06a36b85dd93a8b4',
  'phpunit/php-text-template' => '1.2.1@31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
  'phpunit/php-timer' => '1.0.9@3dcf38ca72b158baf0bc245e9184d3fdffa9c46f',
  'phpunit/php-token-stream' => '1.4.12@1ce90ba27c42e4e44e6d8458241466380b51fa16',
  'phpunit/phpunit' => '4.8.36@46023de9a91eec7dfb06cc56cb4e260017298517',
  'phpunit/phpunit-mock-objects' => '2.3.8@ac8e7a3db35738d56ee9a76e78a4e03d97628983',
  'psr/cache' => '1.0.1@d11b50ad223250cf17b86e38383413f5a6764bf8',
  'psr/container' => '1.0.0@b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
  'psr/link' => '1.0.0@eea8e8662d5cd3ae4517c9b864493f59fca95562',
  'psr/log' => '1.0.2@4ebe3a8bf773a19edfe0a84b6585ba3d401b724d',
  'psr/simple-cache' => '1.0.1@408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
  'sebastian/comparator' => '1.2.4@2b7424b55f5047b47ac6e5ccb20b2aea4011d9be',
  'sebastian/diff' => '1.4.3@7f066a26a962dbe58ddea9f72a4e82874a3975a4',
  'sebastian/environment' => '1.3.8@be2c607e43ce4c89ecd60e75c6a85c126e754aea',
  'sebastian/exporter' => '1.2.2@42c4c2eec485ee3e159ec9884f95b431287edde4',
  'sebastian/global-state' => '1.1.1@bc37d50fea7d017d3d340f230811c9f1d7280af4',
  'sebastian/recursion-context' => '1.0.5@b19cc3298482a335a95f3016d2f8a6950f0fbcd7',
  'sebastian/version' => '1.0.6@58b3a85e7999757d6ad81c787a1fbf5ff6c628c6',
  'sensio/distribution-bundle' => 'v5.0.22@209b11f8cee5bf71986dd703e45e27d3ed7a6d15',
  'sensio/framework-extra-bundle' => 'v5.1.6@bf4940572e43af679aaa13be98f3446a1c237bd8',
  'sensio/generator-bundle' => 'v3.1.7@28cbaa244bd0816fd8908b93f90380bcd7b67a65',
  'sensiolabs/security-checker' => 'v4.1.8@dc270d5fec418cc6ac983671dba5d80ffaffb142',
  'swiftmailer/swiftmailer' => 'v5.4.12@181b89f18a90f8925ef805f950d47a7190e9b950',
  'symfony/monolog-bundle' => 'v3.2.0@8781649349fe418d51d194f8c9d212c0b97c40dd',
  'symfony/phpunit-bridge' => 'v3.4.17@76e013a98031356604e5a730c9eb22713dc4dda4',
  'symfony/polyfill-apcu' => 'v1.9.0@19e1b73bf255265ad0b568f81766ae2a3266d8d2',
  'symfony/polyfill-ctype' => 'v1.9.0@e3d826245268269cd66f8326bd8bc066687b4a19',
  'symfony/polyfill-intl-icu' => 'v1.9.0@f22a90256d577c7ef7efad8df1f0201663d57644',
  'symfony/polyfill-mbstring' => 'v1.9.0@d0cd638f4634c16d8df4508e847f14e9e43168b8',
  'symfony/polyfill-php56' => 'v1.9.0@7b4fc009172cc0196535b0328bd1226284a28000',
  'symfony/polyfill-php70' => 'v1.9.0@1e24b0c4a56d55aaf368763a06c6d1c7d3194934',
  'symfony/polyfill-util' => 'v1.9.0@8e15d04ba3440984d23e7964b2ee1d25c8de1581',
  'symfony/swiftmailer-bundle' => 'v2.6.7@c4808f5169efc05567be983909d00f00521c53ec',
  'symfony/symfony' => 'v3.3.18@e8d2a2ed1f1ad9726388b97a0e76b20405379d1b',
  'twig/extensions' => 'v1.0.1@f91a82ec225e5bb108e01a0f93c9be04f84dcfa0',
  'twig/twig' => 'v1.35.4@7e081e98378a1e78c29cc9eba4aefa5d78a05d2a',
  'webmozart/assert' => '1.3.0@0df1908962e7a3071564e857d86874dad1ef204a',
  'willdurand/jsonp-callback-validator' => 'v1.1.0@1a7d388bb521959e612ef50c5c7b1691b097e909',
  'willdurand/negotiation' => 'v2.3.1@03436ededa67c6e83b9b12defac15384cb399dc9',
  'zendframework/zend-code' => '3.3.1@c21db169075c6ec4b342149f446e7b7b724f95eb',
  'zendframework/zend-eventmanager' => '3.2.1@a5e2583a211f73604691586b8406ff7296a946dd',
  'sample rest api' => 'No version set (parsed as 1.0.0)@',
);

    private function __construct()
    {
    }

    /**
     * @throws \OutOfBoundsException if a version cannot be located
     */
    public static function getVersion(string $packageName) : string
    {
        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new \OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: cannot detect its version'
        );
    }
}
