<?php

namespace ApiBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    protected $product;

    protected function setUp()
    {
        $this->product = array(
            'issn' => '0000-0001',
            'name' => 'Product-' . uniqid()
        ); 
    }

    private function getResponse($method, $url, $options=array(), $access_token=false)
    {
        $client = static::createClient();

        $response = $client->request($method, $url, $options);

        return $client->getResponse();
    }

    public function testQuestionCrud()
    {
        //Test Create Product
        $response = $this->getResponse('POST', '/products', $this->product, true);
        $this->assertEquals(201, $response->getStatusCode());

        //Test Get Products
        $response = $this->getResponse('GET', '/products', array(), true);
        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertGreaterThan(0, count($content));

        $id = $content[0]['issn'];


        //Test Get Product by Id
        $response = $this->getResponse('GET', '/products/' . $id, array(), true);
        $this->assertEquals(200, $response->getStatusCode());

        $this->product['name'] = 'NewName';

        //Test Updating product
        $response = $this->getResponse('PUT', '/products/' . $id, $this->product, true);
        $this->assertEquals(204, $response->getStatusCode());

        //Test Delete Product
        $response = $this->getResponse('DELETE', '/products/' . $id, array(), true);
        $this->assertEquals(201, $response->getStatusCode());
       
    }
}
