<?php

namespace ApiBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CustomerControllerTest extends WebTestCase
{
    protected $customer;

    protected function setUp()
    {
        $this->customer = array(
            'firstName'   => 'First',
            'lastName'    => 'Last',
            'dateOfBirth' => '1990-01-01'
        ); 
    }

    private function getResponse($method, $url, $options=array(), $access_token=false)
    {
        $client = static::createClient();

        $response = $client->request($method, $url, $options);

        return $client->getResponse();
    }

    public function testCustomerCrud()
    {
        //Test Create Customer
        $response = $this->getResponse('POST', '/customers', $this->customer, true);
        $this->assertEquals(201, $response->getStatusCode());
        
        //Test Get Customers
        $response = $this->getResponse('GET', '/customers', array(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertGreaterThan(0, count(json_decode($response->getContent())));
        
        $content = json_decode($response->getContent(), true);
        $id      = $content[0]['uuid'];

        //Test Get Customer by Id
        $response = $this->getResponse('GET', '/customers/' . $id, array(), true);
        $this->assertEquals(200, $response->getStatusCode());

        $this->customer['firstName'] = 'NewName';

        //Test Updating customer
        $response = $this->getResponse('PUT', '/customers/' . $id, $this->customer, true);
        $this->assertEquals(204, $response->getStatusCode());

        //Test Delete Customer
        $response = $this->getResponse('DELETE', '/customers/' . $id, array(), true);
        $this->assertEquals(201, $response->getStatusCode());       
    }
}
