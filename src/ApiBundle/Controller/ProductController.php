<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;


use Symfony\Component\HttpKernel\Exception\HttpException;

use CoreBundle\Entity\Product;
use CoreBundle\Forms\ProductType;

/**
 * Product Controller
 *
 * @Prefix("")
 * @NamePrefix("ApiBundle_ProductController_")
 */
class ProductController extends FOSRestController
{

    /**
     * Create a new Product
     *
     *
     */
    public function postProductsAction()
    {
        $product = new Product();
        $request = $this->get('request_stack')->getCurrentRequest()->request->all();
        $form    = $this->createForm(ProductType::class, $product, ['method' => 'POST']);

        $form->submit($request, 'POST');

        if ($form->isValid()) {
            $product = $form->getData();

            $this->getCustomerRepository()->create($product);
        } else {
            return $form->getErrors();
        }

        $response = new Response(null, 201);
        $response->send();

        return $response;
    }

    /**
     * Get All Products
     *
     * @return array
     */
    public function getProductsAction()
    {
        $products = $this->getProductRepository()->findBy(array('deletedAt' => null));

        return $products;
    }

    /**
     * Get a Product
     *
     * @param  uuid  $uuid
     */
    public function getProductAction($issn)
    {
        $product = $this->getProductRepository()->findOne(array('issn' => $issn, 'deletedAt' => null));

        if(!$product){
            throw new HttpException(404, 'Product not found with id: ' . $issn);
        }

        return $product;
    }

    /**
     * Update a Product
     *
     * @param uuid $uuid
     */
    public function putProductsAction($issn)
    {
        $product = $this->getProductRepository()->findOne(array('issn' => $issn, 'deletedAt' => null));

        if(!$product){
            throw new HttpException(404, 'Product not found with id: ' . $issn);
        }

        $request = $this->get('request_stack')->getCurrentRequest()->request->all();
        $form    = $this->createForm(ProductType::class, $product, ['method' => 'PUT']);

        $form->submit($request, 'PUT');

        if ($form->isValid()) {
            $product = $form->getData();

            $this->getCustomerRepository()->update($product);
        } else {
            return $form->getErrors();
        }

        $response = new Response(null, 204);
        $response->send();

        return $response;
    }

    /**
     * Delete A Product
     *
     * @param  uuid  $uuid
     * @return 
     */
    public function deleteProductsAction($issn)
    {
        $product = $this->getProductRepository()->findOne(array('issn' => $issn, 'deletedAt' => null));

        if(!$product){
            throw new HttpException(404, 'Product not found with id: ' . $issn);
        }

        $this->getProductRepository()->delete($product);

        $response = new Response(null, 204);
        $response->send();

        return $response;
    }


    private function getProductRepository()
    {
        return $this->getDoctrine()->getRepository('CoreBundle:Product');
    }
}
