<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;

use Symfony\Component\HttpKernel\Exception\HttpException;

use CoreBundle\Entity\Customer;
use CoreBundle\Forms\CustomerType;

/**
 * Customer Controller
 *
 * @Prefix("")
 * @NamePrefix("ApiBundle_CustomerController_")
 */
class CustomerController extends Controller
{

    /**
     * Create a new Customer
     *
     *
     */
    public function postCustomersAction()
    {
        $customer = new Customer();
        $request  = $this->get('request_stack')->getCurrentRequest()->request->all();
        $form     = $this->createForm(CustomerType::class, $customer, ['method' => 'POST']);

        $form->submit($request, 'POST');

        if ($form->isValid()) {
            $customer = $form->getData();

            $this->getCustomerRepository()->create($customer);
        } else {
            return $form->getErrors();
            throw new HttpException(409, 'Unable to create customer');
        }

        $response = new Response(null, 201);
        $response->send();

        return $response;
    }

    /**
     * Get All Customers
     *
     * @return array
     */
    public function getCustomersAction()
    {
        $customers = $this->getCustomerRepository()->findBy(array('deletedAt' => null));

        return $customers;
    }

    /**
     * Get a Customer
     *
     * @param  uuid  $uuid
     */
    public function getCustomerAction($uuid)
    {
        $customer = $this->getCustomerRepository()->findOneBy(array('uuid' => $uuid, 'deletedAt' => null));

        if(!$customer){
            throw new HttpException(404, 'Customer not found with id: ' . $uuid);
        }

        return $customer;
    }

    /**
     * Update a Customer
     *
     * @param uuid $uuid
     */
    public function putCustomersAction($uuid)
    {
        $customer = $this->getCustomerRepository()->findOneBy(array('uuid' => $uuid, 'deletedAt' => null));

        if(!$customer){
            throw new HttpException(404, 'Customer not found with id: ' . $uuid);
        }

        $request  = $this->get('request_stack')->getCurrentRequest()->request->all();
        $form     = $this->createForm(CustomerType::class, $customer, ['method' => 'PUT']);

        $form->submit($request, 'PUT');

        if ($form->isValid()) {
            $customer = $form->getData();

            $this->getCustomerRepository()->update($customer);
        } else {
            return $form->getErrors();
        }

        $response = new Response(null, 204);
        $response->send();

        return $response;
    }

    /**
     * Delete A Customer
     *
     * @param  uuid  $uuid
     * @return 
     */
    public function deleteCustomersAction($uuid)
    {
        $customer = $this->getCustomerRepository()->findOneBy(array('uuid' => $uuid, 'deletedAt' => null));

        if(!$customer){
            throw new HttpException(404, 'Customer not found with id: ' . $uuid);
        }
        
        $this->getCustomerRepository()->delete($customer);

        $response = new Response(null, 204);
        $response->send();

        return $response;
    }
 
    /**
     * Fetch All Products Belonging To The Requested Customer
     *
     * @param  uuid  $customer_uuid
     * @return array
     */ 
    public function getCustomersProductsAction($customer_uuid)
    {
        $customer = $this->getCustomerRepository()->findOneBy(array('uuid' => $customer_uuid));

        if(!$customer){
            throw new HttpException(404, 'Customer Not Found');
        } 

        return $customer->getProducts();
    }


    /**
     * Get Customer Repository
     */
    private function getCustomerRepository()
    {
        return $this->getDoctrine()->getRepository('CoreBundle:Customer');
    }
}
