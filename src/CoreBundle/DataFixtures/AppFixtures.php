<?php

namespace CoreBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use CoreBundle\Entity\Customer;

class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $customer = new Customer();

        $customer->setFirstName('John');
        $customer->setLastName('Doe');
        $customer->setDateOfBirth();

        $manager->persist($customer);

        $product = new Product();

        $product->setIssn('1234-5678');
        $product->setName('ProductA');
        $product->setCustomer($customer);

        $manager->persist($product); 

        $manager->flush();
    }
}
