<?php

namespace CoreBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class LogRepository extends DocumentRepository
{

  public function create(Log $log)
  {
    $this->_dm->persist($log);
    $this->_dm->flush();

    return $log;
  }

  public function update(Log $log)
  {
   $this->_dm->persist($log);
    $this->_dm->flush();

    return $log;

  }


  public function delete(Log $log)
  {
    $this->_dm->remove($log);
    $this->_dm->flush();

    return true;

  }
}
