<?php

namespace CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

use CoreBundle\Entity\Product;

/**
 * Product Repsitory
 */
class ProductRepository extends EntityRepository
{

    /**
     * Create a New Product Entity
     *
     * @param  CoreBundle\Entity\Product $product
     * @return CoreBundle\Entity\Product
     */
    public function create(Product $product)
    {
        $this->_em->persist($product);
        $this->_em->flush();

        return $product;
    }

    /**
     * Update a Product Entity
     *
     * @param  CoreBundle\Entity\Product $product
     * @return CoreBundle\Entity\Product
     */
    public function update(Product $product)
    {
        $this->_em->persist($product);
        $this->_em->flush();

        return $product;
    }

    /**
     * Delete Product By Id
     *
     * @param  integer  $issn
     * @return boolean
     */
    public function deleteById($id)
    {
        $product = $this->findOneBy(array('issn' => $id));

        if(!$product){
            throw new \Exception('Unable to find Product with Id: ' . $id);
        }

        return $this->delete($product);
    }

    /**
     * Delete Product Entity
     *
     * @param  CoreBundle\Entity\Product $product
     * @return boolean
     */
    public function delete(Product $product)
    {
        $product->setDeletedAt(new \Datetime('now'));
        $this->_em->persist($product);
        $this->_em->flush();

        return true;
    }

    /**
     * Get All Products Matching The Required Status And Older than 1 Week
     *
     * @param  string  $status
     * @return array
     */
    public function getProductsByStatus($status)
    {
        if($status == null){
            throw new \Exception('Invalid status');
        }

        $now = new \Datetime('now');
        $now->modify('+7 days'); //start date 7 days from now

        $qb = $this->_em->createQueryBuilder();

        $q  = $qb->select(array('p'))
            ->from('CoreBundle:Product', 'p')
            ->where(
                $qb->expr()->gt('p.createdAt', $now)
            )
            ->orderBy('p.createdAt', 'DESC')
            ->getQuery();

        return $q->getResult();
    }
}
