<?php

namespace CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

use CoreBundle\Entity\Customer;

class CustomerRepository extends EntityRepository
{

    /**
     * Create New Customer Entity
     *
     * @param  CoreBundle\Entity\Customer $customer
     * @return CoreBundle\Entity\Customer 
     */
    public function create(Customer $customer)
    {
        $this->_em->persist($customer);
        $this->_em->flush();

        return $customer;
    }

    /**
     * Update Customer Entity
     *
     * @param  CoreBundle\Entity\Customer $customer
     * @return CoreBundle\Entity\Customer 
     */
    public function update(Customer $customer)
    {
        $this->_em->persist($customer);
        $this->_em->flush();

        return $customer;
    }

    /**
     * Delete Customer By Id
     *
     * @param  uuid    $id
     * @return boolean
     */
    public function deleteById($id)
    {
        $customer = $this->findOneBy(array('id' => $id));

        if(!$customer){
            throw new \Exception('Unable to find Customer with Id: ' . $id);
        }

        return $this->delete($customer);
    }

    /**
     * Delete Customer Entity
     *
     * @param  CoreBundle\Entity\Customer $customer
     * @return boolean
     */
    public function delete(Customer $customer)
    {
        $customer->setDeletedAt(new \Datetime('now'));
        $this->_em->persist($customer);
        $this->_em->flush();

        return true;
    }

}
