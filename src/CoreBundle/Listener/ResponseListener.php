<?php

namespace CoreBundle\Listener;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

use CoreBundle\Documents\Log;

class ResponseListener
{
    private $dm;

    public function __construct($dm)
    {
        $this->dm = $dm;
    }

    /**
     * Log all requests with their responses to a mongodb document and local file
     */
    public function onKernelResponse(GetResponseEvent $event)
    {
        try{
            $request  = $event->getRequest();
            $response = $event->getResponse();

            $request_json  = json_encode($request->getCurrentRequest()->request->all());
            $response_json = json_encode($response->getContent());

            $json = array('request' => $request_json, 'response' => $response_json, 'status' => $response->getStatusCode());


            //Save to file 
            file_put_contents('../../var/logs/requests.json', json_encode($json));

            $log = new Log();
            $log->setRequest($request_json);
            $log->setResponse($response_json);
            $log->setStatus($response->getStatusCode());

            //Persist to MongoDB    
            $this->dm->getRepository('CoreBundle:Log')->create($log);
        } catch (\Exception $e) {
            //gracefully handle failure here
        }
        
    }




    public function onRequest(FilterResponseEvent $event)
    {
        $request  = $event->getRequest();
        $response = $event->getResponse();
            
     }
