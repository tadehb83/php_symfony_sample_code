<?php

namespace CoreBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

use CoreBundle\Entity\Customer;

/**
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\ProductRepository")
 * @ORM\Table(name="products")
 */
class Product
{
    /**
     * @Assert\Issn
     * @ORM\Id
     * @ORM\Column(type="string", nullable=false)
     */
    private $issn;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('new', 'pending', 'in review', 'approved', 'inactive', 'deleted')")
     */
    private $status = 'new';

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt = null;

    /**
     * @ORM\ManyToOne(targetEntity="CoreBundle\Entity\Customer", inversedBy="products")
     * @ORM\JoinColumn(name="issd", referencedColumnName="uuid") 
     */
    private $customer;


    public function __construct()
    {
        $this->createdAt = new \Datetime('now');
        $this->updatedAt = new \Datetime('now');
    }

    /**
     * Set ISSN
     *
     * @param integer $issn
     */ 
    public function setIssn($issn)
    {
        $this->issn = $issn;
        return $this;
    }

    /**
     * Get ISSN
     *
     * @return integer
     */
    public function getIssn()
    {
        return $this->issn;
    }

    /**
     * Set Name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set Status
     *
     * @param boolean $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get Status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set Created At
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get Created At
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set Updated At
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * Get Updated At
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set Deleted AT
     *
     * @param datetime $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    /**
     * Get Deleted At
     *
     * @return datetime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set Customer
     *
     * @param CoreBundle\Entity\Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * Get Customer
     *
     * @return CoreBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

}
