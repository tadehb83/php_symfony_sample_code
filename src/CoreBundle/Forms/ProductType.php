<?php

namespace CoreBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use CoreBundle\Forms\CustomerType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('issn',   TextType::class)
            ->add('name',   TextType::class)
            ->add('status', TextType::class, array('empty_data' => 'new'))
        ;
    }

    /**
   * @param OptionsResolver $resolver
   */
    public function configureOptions(OptionsResolver $resolver) 
    {
        $resolver->setDefaults(
            [
                'data_class'      => 'CoreBundle\Entity\Product',
                'csrf_protection' => false,
            ]
        );
    }

    /**
   * @return string
   */
    public function getName() 
    {
        return 'product';
    }
}
