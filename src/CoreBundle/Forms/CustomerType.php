<?php

namespace CoreBundle\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName',   TextType::class)
            ->add('lastName',    TextType::class)
            ->add('dateOfBirth', TextType::class)
            ->add('status',      TextType::class, array('empty_data' => 'new'))
        ;
    }

    /**
   * @param OptionsResolver $resolver
   */
  public function configureOptions(OptionsResolver $resolver) {
    $resolver->setDefaults(
      [
        'data_class'      => 'CoreBundle\Entity\Customer',
        'csrf_protection' => false,
      ]
    );
  }

  /**
   * @return string
   */
  public function getName() {
    return 'customer';
  }
}
