<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ProductCommand extends ContainerAwareCommand
{
    const VALID_STATUS = array('new', 'pending', 'in review', 'approved', 'inactive', 'deleted');

    protected function configure()
    {
        $this->setName('core:products')
            ->setDescription('Fetch Products For a Given Range')
            ->setHelp('Fetch products based on your criteria')
            ->addArgument('status', InputArgument::OPTIONAL, 'Status of desired products');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $status = strtolower($input->getArgument('status'));

        //Validate entered status
        if(!in_array($status, self::VALID_STATUS)){
            $output->writeln('<error>Invalid Status "'. $status . '"</error>');
        } else {

            //Get all products with requested status and send the list via email
            $products = $this->getContainer()->get('product.service')->getProductReport($status);

            $output->writeln('<info>' . count($products) . ' products with status "' . $status . '" submitted to admin</info>');
        }
    }
}
