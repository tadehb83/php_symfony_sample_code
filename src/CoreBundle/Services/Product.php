<?php

namespace CoreBundle\Services;

/**
 * Product Service
 */
class Product
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Get Product Report Based On Requested Status
     *
     * @param  string $status
     * @return array
     */
    public function getProductReport($status)
    {
        try{
            $products = $this->getProductRepository()->getProductsByStatus($status);

            if(count($products) > 0){
                $this->sendReport($products, $status);
            }

            return $products;
        } catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Send Email Report For Requested Product Status
     *
     * @param  array  $products
     * @param  string $status
     */
    private function sendReport(array $products, $status)
    {
        //@TODO: send product list to email
    }

    /**
     * Get Product Repository
     */
    private function getProductRepository()
    {
        return $this->container->get('doctrine')->getManager()->getRepository('CoreBundle:Product');
    }
}
